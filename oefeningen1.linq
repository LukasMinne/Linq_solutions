<Query Kind="Statements" />

// Na basicoperators

List<int> numbers = new List<int> { 20, 35, 17, 105, 90 };

// 1. Getallen deelbaar door 5
numbers.Where(n => n % 5 == 0).Dump("Getallen deelbaar door 5");

// 2. grootste getal
numbers.Max(n => n).Dump("grootste getal");

// 3. voorlaatste getal
numbers.Skip(numbers.Count - 2).Take(1).Dump("voorlaatste getal");

List<string> games = new List<string> { "Dominion", "Manillen", "Schaken", "Kolonisten van Catan", "Cluedo"};

// 4. Uit hoeveel letters bestaan alle spellen samen (spaties niet meetellen)?
int aantal = games
	.Select( g => g.Replace(" ",""))
	.Sum( g => g.Length).Dump("Totaal aantal letters");

// 5. koppel de nummers aan de spellen: nummer is prijs van spel op dezelfde positie
games.Zip(numbers, (naam, prijs) => $"{naam} kost  {prijs.ToString("c")}.") 
	  .Dump("Spellen en prijzen");